<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupAPIController extends Controller
{
    public function getGroupData($id)
    {
        $group = (new \App\Models\Group)->addSelect([
            'oldest_user' => \App\Models\User::select('email')  
            ->whereColumn('group_id', 'group.id')
            ->orderBy('birth_date','desc')
            ->limit(1)])
            ->addSelect([
            'youngest_user' => \App\Models\User::select('email')
            ->whereColumn('group_id', 'group.id')
            ->orderBy('birth_date','asc')
            ->limit(1)])
            ->addSelect(['avg_age' => \App\Models\User::select(\DB::raw('ROUND(AVG(TIMESTAMPDIFF(YEAR, birth_date, NOW()))) '))
            ->whereColumn('group_id', 'group.id')])->find($id);    
        $group->child_groups = $group->child_groups()->get();
        $group->get();
        return response()->json($group);
    }
}
