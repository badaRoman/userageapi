<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Group extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'parent_id',
    ];
    protected $table = 'group';
    
    public function childrenGroups()
    {
        $result = $this->hasMany('App\Models\Group', 'parent_id', 'id');       
        return $result;
    }

    public function child_groups()
    {        
        $result = $this->childrenGroups()->with('child_groups')->addSelect([
            'oldest_user' => User::select('email')  
            ->whereColumn('group_id', 'group.id')
            ->orderBy('birth_date','desc')
            ->limit(1)])
            ->addSelect([
            'youngest_user' => User::select('email')
            ->whereColumn('group_id', 'group.id')
            ->orderBy('birth_date','asc')
            ->limit(1)])
            ->addSelect(['avg_age' => User::select(\DB::raw('ROUND(AVG(TIMESTAMPDIFF(YEAR, birth_date, NOW()))) '))
            ->whereColumn('group_id', 'group.id')]);
        return $result;        
    }    
}
