<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        $parentId = DB::table('group')->insertGetId([
            'name' => 'Root'//Str::random(10)
        ]);
                       
        \App\Models\User::factory(5)->create([
            'group_id' => $parentId
        ]);
        $params = [
            'name' => 'Group ',
            'parent_id' => $parentId
        ]; 
        self::seedChildren($params, 0);
    }
    private function seedChildren($params, $curDepth){   
        if ($curDepth < 3){
            for ($i=1; $i<4; $i++){
                $parentId = DB::table('group')->insertGetId([
                    'name' => $params['name'].$i,
                    'parent_id' => $params['parent_id']
                ]);
                \App\Models\User::factory(5)->create([
                    'group_id' => $parentId
                ]);
                $newParams = [
                    'name' => $params['name'].$i.'.',
                    'parent_id' => $parentId
                ];                
                self::seedChildren($newParams, $curDepth+1);
            }            
        }
    }
}
