<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CheckAPIRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Carbon::createFromTimestamp(-1)->toDateTimeString(); 
        if (!$request->hasHeader('app-key') || 
            $request->header('app-key') != env(APP_KEY) || 
            !$request->hasHeader('app-timestamp') ||
            Carbon::now()->diffInSeconds(Carbon::createFromTimeStamp($request->header('app-timestamp'))) > Config::get('constants.API_TIMESTAMP_DIFFERENCE') ) 
        {
            abort(404);
        }        
        return $next($request);
    }
}
